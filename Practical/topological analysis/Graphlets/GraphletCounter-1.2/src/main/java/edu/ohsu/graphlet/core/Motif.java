package edu.ohsu.graphlet.core;

import java.util.List;
import java.util.ArrayList;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents a list of motifs of a single type within a network identified by some external process
 * (e.g. a motif-finding program)
 */
public class Motif {
    /**
     * An identifier for the type of the motifs contained in the list.
     */
    private String subgraphId;

    /**
     * Individual motifs from the network
     */
    private List<MotifInstance> instances = new ArrayList<MotifInstance>();

    public String getSubgraphId() {
        return subgraphId;
    }

    public void setSubgraphId(String subgraphId) {
        this.subgraphId = subgraphId;
    }

    public List<MotifInstance> getInstances() {
        return instances;
    }

    public void setInstances(List<MotifInstance> members) {
        this.instances = members;
    }
}
