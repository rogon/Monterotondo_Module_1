package edu.ohsu.graphlet.core;

import static org.junit.Assert.*;
import org.junit.Test;
import edu.ohsu.graphlet.core.Node;
import edu.ohsu.graphlet.core.NodeImpl;
import edu.ohsu.graphlet.core.GraphletCounter;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GraphletCounterTest {
    
    @Test
    public void testGetGraphletSpectrum() {

        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        int[] dSpectrum = new int[GraphletCounter.NUM_ORBITS];

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        Node b = new NodeImpl("b");
        a.addNeighbor(b);
        aSpectrum[0] = 1;

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        Node c = new NodeImpl("c");
        a.addNeighbor(c);
        aSpectrum[0] = 2;
        aSpectrum[2] = 1;

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        c.addNeighbor(b);
        aSpectrum[3] = 1;
        aSpectrum[2] = 0;

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        Node d = new NodeImpl("d");
        b.addNeighbor(d);
        aSpectrum[1] = 1;
        aSpectrum[10] = 1;

        dSpectrum[0] = 1;
        dSpectrum[1] = 2;
        dSpectrum[9] = 1;

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

        Node e = new NodeImpl("e");
        d.addNeighbor(e);
        aSpectrum[4] = 1;   // a-b-d-e
        aSpectrum[29] = 1;  // a-b-c-d-e

        dSpectrum[0] = 2;
        dSpectrum[1] = 3;
        dSpectrum[2] = 1;
        dSpectrum[5] = 1;
        // todo: dSpectrum[28] = 1; // a-b-c-d-e

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        e.addNeighbor(c);
        aSpectrum[1] = 2;
        aSpectrum[4] = 2; // a-c-e-d, a-b-d-e
        aSpectrum[10] = 2;
        aSpectrum[29] = 0;
        aSpectrum[52] = 1; // a-c-b-d-e

        dSpectrum[1] = 3;
        dSpectrum[4] = 1;
        dSpectrum[5] = 1;
        dSpectrum[8] = 1;
        //todo: dSpectrum[28] = 1;
        dSpectrum[51] = 1; // d-e-c-a-b

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);
        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

        Node f = new NodeImpl("f");
        d.addNeighbor(f);

        aSpectrum[4] = 3;
        aSpectrum[15] = 1; // a-c-e-d-f
        aSpectrum[18] = 1; // a-b-d-f-e
        aSpectrum[29] = 1; // a-c-b-d-f

        dSpectrum[0] = 3;
        dSpectrum[2] = 3;
        dSpectrum[5] = 4;
        dSpectrum[7] = 1;
        dSpectrum[16] = 1; // f-d-e-c-a
        dSpectrum[21] = 1; // a-b-d-f-e
        dSpectrum[28] = 1; // f-d-b-a-c
        dSpectrum[38] = 1; // f-d-e-c-b

        results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);
        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

        int[] fSpectrum = new int[GraphletCounter.NUM_ORBITS];
        fSpectrum[0] = 1;
        fSpectrum[1] = 2;
        fSpectrum[4] = 3;
        fSpectrum[6] = 1;
        fSpectrum[15] = 1; // f-d-e-c-a
        fSpectrum[19] = 1; // f-e-d-b-a
        fSpectrum[27] = 1;
        fSpectrum[35] = 1; // f-e-d-b-c

        results = counter.getGraphletSignature(f);
        assertSame(fSpectrum, results);

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 3;
        bSpectrum[2] = 2;
        bSpectrum[3] = 1;
        bSpectrum[5] = 3; // f-d-b-a, f-d-b-c, e-d-b-a
        bSpectrum[6] = 1;       
        bSpectrum[8] = 1;
        bSpectrum[10] = 1;
        bSpectrum[11] = 1;
        bSpectrum[20] = 1; // a-b-d-f-e
        bSpectrum[30] = 1; // f-d-b-a-c
        bSpectrum[37] = 1; // b-c-e-d-f
        bSpectrum[53] = 1; // b-d-e-c-a

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        Node g = new NodeImpl("g");
        f.addNeighbor(g);
        g.addNeighbor(e);
        f.addNeighbor(e);

        fSpectrum[0] += 2;
        fSpectrum[2] = 1; // d-e-f
        fSpectrum[3] = 2;
        fSpectrum[4] += 1; // 4: f-d-b-a, f-d-b-c, f-e-c-a, f-e-c-b
        fSpectrum[5] += 1;
        fSpectrum[6] = 0;
        fSpectrum[10] = 3; // f-g-e-c, f-d-e-c, f-e-d-b
        fSpectrum[13] = 1; // f-g-e-d
        fSpectrum[15] = 0;
        fSpectrum[16] = 2; // g-f-d-b-c, g-f-d-b-a
        fSpectrum[19] = 0;
        fSpectrum[27] = 2; // f-e-c-b-a, f-d-b-a-c
        fSpectrum[29] = 4; // f-g-e-c-a, f-d-e-c-a, f-g-e-c-b, f-e-d-b-a
        fSpectrum[35] = 0;
        fSpectrum[41] = 1; // f-g-d-e-c
        fSpectrum[48] = 1; // f-g-e-d-b
        fSpectrum[52] = 1; // f-e-c-b-d


        dSpectrum[1] = 5; // d-e-g, d-e-f, d-b-c, d-b-a, d-e-c
        dSpectrum[2] = 2; //f-d-b, b-d-e
        dSpectrum[3] = 1; //d-e-f
        dSpectrum[5] = 5; // f-d-b-a, f-d-b-c, g-f-d-b, e-d-b-a, g-e-d-b
        dSpectrum[6] = 1; // d-e-c-g
        dSpectrum[7] = 0;
        dSpectrum[10] = 1; //d-f-e-c
        dSpectrum[11] = 1; //b-d-e-f
        dSpectrum[12] = 1; // d-e-g-f;
        dSpectrum[16] = 0;
        dSpectrum[17] = 3; // g-f-d-b-a, g-f-d-b-c, g-e-d-b-a
        dSpectrum[19] = 1; // d-g-e-c-a
        dSpectrum[21] = 0;
        dSpectrum[29] = 1; // d-f-e-c-a
        dSpectrum[30] = 1; // f-e-d-b-a
        dSpectrum[37] = 1; // d-b-c-e-f
        dSpectrum[38] = 0;
        dSpectrum[40] = 1; // d-f-g-e-c
        dSpectrum[47] = 1; // f-g-e-d-b
        dSpectrum[53] = 1; // d-f-e-c-b

        results = counter.getGraphletSignature(f);
        assertSame(fSpectrum, results);

        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

        g.addNeighbor(d);
        fSpectrum[2] = 0;
        fSpectrum[3] = 3; // f-g-e, f-d-e, f-d-g
        fSpectrum[5] = 0;
        fSpectrum[10] = 4; // f-g-e-c, f-d-e-c, f-e-d-b, f-g-d-b
        fSpectrum[13] = 0;
        fSpectrum[14] = 1; // f-g-e-d
        fSpectrum[16] = 0;
        fSpectrum[29] = 6; // f-g-e-c-a, f-d-e-c-a, f-g-e-c-b, f-e-d-b-a, f-g-d-b-a, f-g-d-b-c
        fSpectrum[41] = 0;
        fSpectrum[48] = 0;
        fSpectrum[57] = 2; // f-g-e-d-b, f-g-d-e-c

        results = counter.getGraphletSignature(f);
        assertSame(fSpectrum, results);

    }

    @Test
    public void testG9() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 1;
        aSpectrum[4] = 1;
        aSpectrum[15] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 2;
        bSpectrum[1] = 1;
        bSpectrum[2] = 1;
        bSpectrum[4] = 1;
        bSpectrum[5] = 1;
        bSpectrum[16] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 2;
        cSpectrum[2] = 1;
        cSpectrum[5] = 2;
        cSpectrum[17] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);
        
        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

    }

    @Test
    public void testG10() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 1;
        aSpectrum[4] = 2;
        aSpectrum[18] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 2;
        bSpectrum[1] = 2;
        bSpectrum[2] = 1;
        bSpectrum[5] = 2;
        bSpectrum[6] = 1;
        bSpectrum[20] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 3;
        cSpectrum[1] = 1;
        cSpectrum[2] = 3;
        cSpectrum[5] = 2;
        cSpectrum[7] = 1;
        cSpectrum[21] = 1;

        int[] dSpectrum = new int[GraphletCounter.NUM_ORBITS];
        dSpectrum[0] = 1;
        dSpectrum[1] = 2;
        dSpectrum[4] = 1;
        dSpectrum[6] = 1;
        dSpectrum[19] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

    }

    @Test
    public void testG11() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);

        b.addNeighbor(c);
        b.addNeighbor(d);
        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 3;
        aSpectrum[6] = 3;
        aSpectrum[22] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = (int) GraphletCounter.choose(4,2);
        bSpectrum[7] = 4;
        bSpectrum[23] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);
    }

    @Test
    public void testG12() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        b.addNeighbor(d);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 2;
        aSpectrum[4] = 1;
        aSpectrum[9] = 1;
        aSpectrum[24] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 1;
        bSpectrum[2] = 2;
        bSpectrum[3] = 1;
        bSpectrum[5] = 1;
        bSpectrum[10] = 1;
        bSpectrum[11] = 1;
        bSpectrum[26] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 2;
        cSpectrum[3] = 1;
        cSpectrum[10] = 2;
        cSpectrum[25] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

    }

    @Test
    public void testG13() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 1;
        aSpectrum[4] = 2;
        aSpectrum[27] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 2;
        bSpectrum[1] = 2;
        bSpectrum[2] = 1;
        bSpectrum[5] = 2;
        bSpectrum[9] = 1;
        bSpectrum[28] = 1;

        int[] eSpectrum = new int[GraphletCounter.NUM_ORBITS];
        eSpectrum[0] = 2;
        eSpectrum[1] = 1;
        eSpectrum[3] = 1;
        eSpectrum[4] = 1;
        eSpectrum[10] = 1;
        eSpectrum[29] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 3;
        cSpectrum[1] = 1;
        cSpectrum[2] = 2;
        cSpectrum[3] = 1;
        cSpectrum[5] = 2;
        cSpectrum[11] = 1;
        cSpectrum[30] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(e);
        assertSame(eSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);
    }

    @Test
    public void testG14() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);

        b.addNeighbor(c);
        b.addNeighbor(d);
        b.addNeighbor(e);

        c.addNeighbor(d);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 3;
        aSpectrum[6] = 2;
        aSpectrum[9] = 1;
        aSpectrum[31] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 5;
        bSpectrum[3] = 1;
        bSpectrum[7] = 2;
        bSpectrum[11] = 2;
        bSpectrum[33] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 2;
        cSpectrum[3] = 1;
        cSpectrum[6] = 1;
        cSpectrum[10] = 2;
        cSpectrum[32] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

    }

    
    @Test
    public void testG15() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);        
        a.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 2;
        aSpectrum[2] = 1;
        aSpectrum[4] = 2;
        aSpectrum[5] = 2;
        aSpectrum[34] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

    }

    @Test
    public void testG16() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);
        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 2;
        aSpectrum[4] = 2;
        aSpectrum[6] = 1;
        aSpectrum[35] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 2;
        bSpectrum[2] = 3;
        bSpectrum[5] = 2;
        bSpectrum[7] = 1;
        bSpectrum[8] = 1;
        bSpectrum[38] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 3;
        cSpectrum[2] = 1;
        cSpectrum[5] = 1;
        cSpectrum[6] = 1;
        cSpectrum[8] = 1;
        cSpectrum[37] = 1;

        int[] dSpectrum = new int[GraphletCounter.NUM_ORBITS];
        dSpectrum[0] = 2;
        dSpectrum[1] = 2;
        dSpectrum[2] = 1;
        dSpectrum[4] = 2;
        dSpectrum[8] = 1;
        dSpectrum[36] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

    }

    @Test
    public void testG17() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        b.addNeighbor(d);
        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 3;
        aSpectrum[6] = 1;
        aSpectrum[9] = 2;
        aSpectrum[39] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 4;
        bSpectrum[3] = 2;
        bSpectrum[7] = 1;
        bSpectrum[11] = 2;
        bSpectrum[13] = 1;
        bSpectrum[42] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 3;
        cSpectrum[3] = 1;
        cSpectrum[6] = 1;
        cSpectrum[10] = 1;
        cSpectrum[12] = 1;
        cSpectrum[40] = 1;

        int[] dSpectrum = new int[GraphletCounter.NUM_ORBITS];
        dSpectrum[0] = 3;
        dSpectrum[1] = 1;
        dSpectrum[2] = 1;
        dSpectrum[3] = 2;
        dSpectrum[10] = 2;
        dSpectrum[13] = 1;
        dSpectrum[41] = 1;        

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

    }

    @Test
    public void testG18() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        a.addNeighbor(c);

        b.addNeighbor(c);

        c.addNeighbor(d);
        c.addNeighbor(e);

        d.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 2;
        aSpectrum[3] = 1;
        aSpectrum[9] = 1;
        aSpectrum[10] = 2;
        aSpectrum[43] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 4;
        cSpectrum[2] = 4;
        cSpectrum[3] = 2;
        cSpectrum[11] = 4;
        cSpectrum[44] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);
    }


    @Test
    public void testG19() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        c.addNeighbor(e);
        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 2;
        aSpectrum[4] = 2;
        aSpectrum[9] = 1;
        aSpectrum[45] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 2;
        bSpectrum[2] = 2;
        bSpectrum[3] = 1;
        bSpectrum[5] = 2;
        bSpectrum[11] = 1;
        bSpectrum[12] = 1;
        bSpectrum[47] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 3;
        cSpectrum[1] = 1;
        cSpectrum[2] = 1;
        cSpectrum[3] = 2;
        cSpectrum[5] = 1;
        cSpectrum[10] = 1;
        cSpectrum[13] = 1;
        cSpectrum[48] = 1;

        int[] dSpectrum = new int[GraphletCounter.NUM_ORBITS];
        dSpectrum[0] = 2;
        dSpectrum[1] = 2;
        dSpectrum[3] = 1;
        dSpectrum[4] = 2;
        dSpectrum[12] = 1;
        dSpectrum[46] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

        results = counter.getGraphletSignature(d);
        assertSame(dSpectrum, results);

    }


    @Test
    public void testG20() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(d);

        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 4;
        aSpectrum[2] = 1;
        aSpectrum[6] = 2;
        aSpectrum[8] = 2;
        aSpectrum[49] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 3;
        bSpectrum[2] = 3;
        bSpectrum[7] = 1;
        bSpectrum[8] = 3;
        bSpectrum[50] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);
    }

    @Test
    public void testG21() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(e);

        b.addNeighbor(d);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 3;
        aSpectrum[2] = 1;
        aSpectrum[4] = 1;
        aSpectrum[5] = 1;
        aSpectrum[8] = 1;
        aSpectrum[9] = 1;
        aSpectrum[51] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 2;
        bSpectrum[2] = 2;
        bSpectrum[3] = 1;
        bSpectrum[5] = 1;
        bSpectrum[8] = 1;
        bSpectrum[10] = 1;
        bSpectrum[11] = 1;
        bSpectrum[53] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 2;
        cSpectrum[1] = 2;
        cSpectrum[3] = 1;
        cSpectrum[4] = 2;
        cSpectrum[10] = 2;
        cSpectrum[52] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

    }

    @Test
    public void testG22() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(d);

        b.addNeighbor(d);
        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 4;
        aSpectrum[3] = 1;
        aSpectrum[6] = 2;
        aSpectrum[12] = 2;
        aSpectrum[54] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 3;
        bSpectrum[3] = 3;
        bSpectrum[7] = 1;
        bSpectrum[13] = 3;
        bSpectrum[55] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

    }

    @Test
    public void testG23() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        b.addNeighbor(d);
        b.addNeighbor(e);

        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 1;
        aSpectrum[1] = 3;
        aSpectrum[9] = 3;
        aSpectrum[56] = 1;

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 3;
        bSpectrum[3] = 3;
        bSpectrum[11] = 3;
        bSpectrum[14] = 1;
        bSpectrum[58] = 1;

        int[] eSpectrum = new int[GraphletCounter.NUM_ORBITS];
        eSpectrum[0] = 3;
        eSpectrum[1] = 1;
        eSpectrum[3] = 3;
        eSpectrum[10] = 2;
        eSpectrum[14] = 1;
        eSpectrum[57] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        results = counter.getGraphletSignature(e);
        assertSame(eSpectrum, results);

    }

    @Test
    public void testG24() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(e);

        b.addNeighbor(d);
        b.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 3;
        aSpectrum[3] = 1;
        aSpectrum[4] = 1;
        aSpectrum[9] = 1;
        aSpectrum[10] = 1;
        aSpectrum[12] = 1;
        aSpectrum[59] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 3;
        bSpectrum[3] = 3;
        bSpectrum[11] = 2;
        bSpectrum[13] = 2;
        bSpectrum[61] = 1;

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        int[] eSpectrum = new int[GraphletCounter.NUM_ORBITS];
        eSpectrum[0] = 3;
        eSpectrum[1] = 2;
        eSpectrum[2] = 1;
        eSpectrum[3] = 2;
        eSpectrum[5] = 1;
        eSpectrum[10] = 1;
        eSpectrum[12] = 1;
        eSpectrum[13] = 1;
        eSpectrum[60] = 1;

        results = counter.getGraphletSignature(e);
        assertSame(eSpectrum, results);

    }

    @Test
    public void testG25() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(d);

        b.addNeighbor(e);

        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 4;
        aSpectrum[2] = 1;
        aSpectrum[8] = 2;
        aSpectrum[9] = 2;
        aSpectrum[62] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 3;
        bSpectrum[1] = 3;
        bSpectrum[2] = 2;
        bSpectrum[3] = 1;
        bSpectrum[8] = 2;
        bSpectrum[11] = 1;
        bSpectrum[12] = 1;
        bSpectrum[63] = 1;

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

        int[] eSpectrum = new int[GraphletCounter.NUM_ORBITS];
        eSpectrum[0] = 3;
        eSpectrum[1] = 2;
        eSpectrum[2] = 1;
        eSpectrum[3] = 2;
        eSpectrum[8] = 1;
        eSpectrum[10] = 2;
        eSpectrum[13] = 1;
        eSpectrum[64] = 1;

        results = counter.getGraphletSignature(e);
        assertSame(eSpectrum, results);

    }

    @Test
    public void testG26() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(d);

        b.addNeighbor(d);
        b.addNeighbor(e);

        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 2;
        aSpectrum[1] = 4;
        aSpectrum[3] = 1;
        aSpectrum[9] = 2;
        aSpectrum[12] = 2;
        aSpectrum[65] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 3;
        cSpectrum[1] = 2;
        cSpectrum[3] = 3;
        cSpectrum[10] = 2;
        cSpectrum[12] = 1;
        cSpectrum[14] = 1;
        cSpectrum[66] = 1;

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 2;
        bSpectrum[3] = 4;
        bSpectrum[11] = 1;
        bSpectrum[13] = 2;
        bSpectrum[14] = 1;
        bSpectrum[67] = 1;

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

    }

    @Test
    public void testG27() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(c);
        a.addNeighbor(d);
        
        b.addNeighbor(e);

        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 3;
        aSpectrum[1] = 3;
        aSpectrum[2] = 1;
        aSpectrum[3] = 2;
        aSpectrum[8] = 1;
        aSpectrum[12] = 2;
        aSpectrum[13] = 1;
        aSpectrum[68] = 1;

        int[] cSpectrum = new int[GraphletCounter.NUM_ORBITS];
        cSpectrum[0] = 4;
        cSpectrum[2] = 2;
        cSpectrum[3] = 4;
        cSpectrum[13] = 4;
        cSpectrum[69] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        results = counter.getGraphletSignature(c);
        assertSame(cSpectrum, results);

    }

    @Test
    public void testG28() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(d);
        a.addNeighbor(e);

        b.addNeighbor(d);
        b.addNeighbor(e);

        c.addNeighbor(e);

        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 3;
        aSpectrum[1] = 3;
        aSpectrum[3] = 3;
        aSpectrum[12] = 3;
        aSpectrum[14] = 1;
        aSpectrum[70] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

        int[] bSpectrum = new int[GraphletCounter.NUM_ORBITS];
        bSpectrum[0] = 4;
        bSpectrum[2] = 1;
        bSpectrum[3] = 5;
        bSpectrum[13] = 2;
        bSpectrum[14] = 2;
        bSpectrum[71] = 1;

        results = counter.getGraphletSignature(b);
        assertSame(bSpectrum, results);

    }


    @Test
    public void testG29() throws Exception {
        GraphletCounter counter = new GraphletCounter();

        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(d);
        d.addNeighbor(e);

        a.addNeighbor(c);
        a.addNeighbor(d);
        a.addNeighbor(e);

        b.addNeighbor(d);
        b.addNeighbor(e);

        c.addNeighbor(e);


        int[] aSpectrum = new int[GraphletCounter.NUM_ORBITS];
        aSpectrum[0] = 4;
        aSpectrum[3] = (int) GraphletCounter.choose(4,2);
        aSpectrum[14] = 4;
        aSpectrum[72] = 1;

        int[] results = counter.getGraphletSignature(a);
        assertSame(aSpectrum, results);

    }


    public static void assertSame(int[] expectedResults, int[] results) {
        assertEquals(expectedResults.length, results.length);
        for (int i = 0; i < expectedResults.length; i++) {
            assertEquals("differed at position " + i, expectedResults[i], results[i]);            
        }
    }
}
