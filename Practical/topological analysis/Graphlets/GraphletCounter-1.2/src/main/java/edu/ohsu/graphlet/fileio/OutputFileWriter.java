package edu.ohsu.graphlet.fileio;

import edu.ohsu.graphlet.core.CountResults;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class handles the File IO aspect of printing graphlet count results to a file.
 */
public class OutputFileWriter {
    private File outputFile;
    private CountResults results;

    public OutputFileWriter(File outputFile, CountResults results) {
        this.outputFile = outputFile;
        this.results = results;
    }

    public void saveFile() {
        FileWriter writer = null;
        try {
            writer = new FileWriter(outputFile);
            results.print(writer);
        } catch (IOException e) {

        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }

}
