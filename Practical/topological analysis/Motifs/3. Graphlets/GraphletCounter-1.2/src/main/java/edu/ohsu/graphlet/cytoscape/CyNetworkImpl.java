package edu.ohsu.graphlet.cytoscape;

import cytoscape.CyNetwork;
import cytoscape.Cytoscape;
import cytoscape.data.Semantics;
import edu.ohsu.graphlet.core.Network;
import edu.ohsu.graphlet.core.Node;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implementation of edu.ohsu.graphlet.core.Network using a Cytoscape network as backing.
 */
public class CyNetworkImpl implements Network {

    CyNetwork cyNetwork;
    Map<String, CyNodeImpl> nodesById = new HashMap<String, CyNodeImpl>();

    /**
     * Construct a graphlet counter network based on the Cystoscape network.
     * @param cyNetwork
     */
    public CyNetworkImpl(CyNetwork cyNetwork) {
        this.cyNetwork = cyNetwork;
    }

    /**
     * Returns the node identified by Id.
     * @param id
     * @return
     */
    public Node getNode(String id) {
        if (! nodesById.containsKey(id)) {
            giny.model.Node cyNode = Cytoscape.getCyNode(id);
            CyNodeImpl node = new CyNodeImpl(cyNetwork, cyNode);
            nodesById.put(id, node);
        }
        return nodesById.get(id);
    }

    /**
     * Creates a node with the given id.
     * @param id
     * @return
     */
    public Node createNode(String id) {
        giny.model.Node cyNode = Cytoscape.getCyNode(id, true);
        CyNodeImpl node = new CyNodeImpl(cyNetwork, cyNode);
        cyNetwork.addNode(cyNode);
        nodesById.put(id, node);
        return node;
    }

    /**
     * Not Implemented
     * @param node
     */
    public void addNode(Node node) {
        throw new UnsupportedOperationException();
    }

    /**
     * Adds a new undirected edge between node1 and node2, using Cytoscape interaction type "pp"
     * @param node1
     * @param node2
     */
    public void addUndirectedEdge(Node node1, Node node2) {
        cyNetwork.addEdge(Cytoscape.getCyEdge(((CyNodeImpl) node1).getCyNode(), ((CyNodeImpl) node1).getCyNode(), Semantics.INTERACTION, "pp", true));
    }

    /**
     * Hides the given node from the rest of the network
     * @param node
     */
    public void hideNode(Node node) {
        CyNodeImpl cyNode = (CyNodeImpl) node;
        cyNetwork.hideNode(cyNode.getCyNode());
    }

    /**
     * Returns an iterator over all nodes in the network.
     * @return
     */
    public Iterator<Node> nodeIterator() {
        Iterator cyNodesIterator = cyNetwork.nodesIterator();
        return new NodeIterator(cyNodesIterator);
    }

    class NodeIterator implements Iterator {
        private Iterator cyIterator;

        NodeIterator(Iterator cyIterator) {
            this.cyIterator = cyIterator;
        }

        public boolean hasNext() {
            return cyIterator.hasNext();
        }

        public Object next() {
            return new CyNodeImpl(cyNetwork, (giny.model.Node) cyIterator.next());
        }

        public void remove() {
            cyIterator.remove();
        }
    }
}
