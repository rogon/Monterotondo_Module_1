package edu.ohsu.graphlet.fileio;

import edu.ohsu.graphlet.core.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Main class for the standalone command line implementation of GraphletCounter.
 */
public class GC {

    /**
     * Parses command line args and executes a graphlet count.
     * @param args
     */
    public static void main(String[] args) throws MotifFileFormatException {
        try {
            boolean countAllNodes = false;
            boolean countNodes = true;
            boolean countMotifs = false;
            if (args.length < 2) {
                printUsage();
                return;
            }
            String networkFileName = args[0];
            String command = args[1];
            String motifFileName = null;
            String nodeListFileName = null;

            if (command.equals("-all")) {
                if (args.length != 2)  {
                    printUsage();
                    return;
                }
                countAllNodes = true;
                countNodes = true;
            } else if (command.equals("-list")) {
                if (args.length != 3)  {
                    printUsage();
                    return;
                }
                countNodes = true;
                nodeListFileName = args[2];
            } else if (command.equals("-motifs")) {
                if (args.length != 3)  {
                    printUsage();
                    return;
                }
                countMotifs = true;
                countNodes = false;
                motifFileName = args[2];
            }

            File networkFile = new File(networkFileName);
            SimpleGraphFileReader simpleGraphFileReader = new SimpleGraphFileReader();
            NetworkImpl network = (NetworkImpl) simpleGraphFileReader.readNetworkFile(new FileReader(networkFile));
            GraphletCounter counter = new GraphletCounter();

            CountResults results = null;
            if (countNodes) {
                results = new CountResults();
                Iterator nodeIterator;

                if (countAllNodes) {
                    nodeIterator = network.nodeIterator();
                } else {
                    Set<String> selectedNodeIds = new NodeListFileReader().readNodeListFile(new FileReader(new File(nodeListFileName)));
                    nodeIterator = new NodeListIterator(selectedNodeIds, network);
                }

                while (nodeIterator.hasNext()) {
                    Node node = (Node) nodeIterator.next();
                    int[] counts = counter.getGraphletSignature(node);
                    node.setCounts(counts);
                    results.put(node.getId(), node);
                }
            } else if (countMotifs) {
                results = new MotifCountResults();
                MotifFileReader motifReader = new MFinderMembersFileReader();
                List<Motif> motifList = motifReader.readMembersFile(new FileReader(new File(motifFileName)));
                MotifInstanceCollapser motifInstanceCollapser = new MotifInstanceCollapser();
                for (Motif motif : motifList) {
                    for (MotifInstance motifInstance : motif.getInstances()) {
                        motifInstanceCollapser.collapseNodes(motifInstance, network);
                        Node motifNode = network.getNode(motifInstance.getMotifInstanceName());
                        motifInstance.setCounts(counter.getGraphletSignature(motifNode));
                        results.put(motifInstance.getId(), motifInstance);
                        network.removeNode(motifNode.getId());
                        for (String member : motifInstance.getMembers()) {
                            network.restoreNode(member);
                        }
                    }
                }
            }

            OutputStreamWriter writer = new OutputStreamWriter(System.out);
            results.print(writer);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void printUsage() {
        System.err.println("Usage: GC <networkfile> -[all|motifs|list] [motiffile|listfile]");
    }
}
