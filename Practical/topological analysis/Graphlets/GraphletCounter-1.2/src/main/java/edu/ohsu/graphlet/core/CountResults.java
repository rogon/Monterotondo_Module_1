package edu.ohsu.graphlet.core;

import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class stores a set of count results for nodes. It is a map of identifiers
 * to objects of type HasGraphletSignature (which could be nodes or motifs) that
 * can then be queried to get the actual graphlet counts.
 */
public class CountResults extends HashMap<String, HasGraphletSignature> {

    /**
     * Prints the count results in a pretty format to the given writer
     * @param writer
     * @throws IOException
     */
    public void print(Writer writer) throws IOException {
        writer.append("NODE\tSUBGRPAH_ID\tORBIT\tRAW_COUNT\tWEIGHTED_COUNT\n");
        DecimalFormat decimalFormat = new DecimalFormat("#.#####");
        java.util.List<String> nodeList = new ArrayList<String>();
        for (String node : this.keySet()) {
            nodeList.add(node);
        }
        Collections.sort(nodeList);
        for (String node : nodeList) {
            for (int i = 0; i < GraphletCounter.NUM_ORBITS; i++) {
                StringBuffer b = new StringBuffer();
                b.append(get(node).getId());
                b.append("\t" + getSubgraphId(node) + "\t");
                b.append(i);
                b.append("\t");
                b.append(get(node).getCounts()[i]);
                b.append("\t");
                b.append(decimalFormat.format(get(node).getWeightedCounts()[i]));
                b.append("\n");
                writer.append(b);
            }
        }
    }

    /**
     * Returns the subgraph ID (motif type) associated with this node. For single nodes, there is no
     * motif type, and so "NA" is returned.
     * @param node
     * @return
     */
    protected String getSubgraphId(String id) {
        return "NA";
    }

}
