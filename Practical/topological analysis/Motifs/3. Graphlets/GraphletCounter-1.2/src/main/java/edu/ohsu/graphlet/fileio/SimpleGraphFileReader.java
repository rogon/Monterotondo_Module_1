package edu.ohsu.graphlet.fileio;

import edu.ohsu.graphlet.core.Network;
import edu.ohsu.graphlet.core.NetworkImpl;
import edu.ohsu.graphlet.core.Node;
import edu.ohsu.graphlet.core.NodeImpl;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Reads a simple textual representation of an undirected network and returns a network backed by a
 * edu.ohsu.graphlet.core.NetworkImpl. The file should be in simple adjacency list format, with each line
 * containing two node Ids delimited by whitespace:
 *
 * node1 node2
 *
 * The resulting network will contain both node1 and node2 and the two nodes will be connected by an edge
 */
public class SimpleGraphFileReader {

    /**
     * Reads a simple textual representation of an undirected network and returns a network backed by a
     * edu.ohsu.graphlet.core.NetworkImpl. The file should be in simple adjacency list format, with each line
     * containing two node Ids delimited by whitespace:
     *
     * node1 node2
     *
     * The resulting network will contain both node1 and node2 and the two nodes will be connected by an edge.
     * GraphletCounter currently does not deal with self-loops so we remove them here.
     */
    public Network readNetworkFile(Reader reader) throws IOException {
        Pattern pattern = Pattern.compile("^(\\w+)\\s+(\\w+).*");

        Network network = new NetworkImpl();
        BufferedReader br = new BufferedReader(reader);
        String line = br.readLine();
        while (line != null) {
            Matcher m = pattern.matcher(line);
            if (m.find()) {
                String node1id = m.group(1);
                String node2id = m.group(2);
                if (! node1id.equals(node2id) )  {
                    Node node1 = createOrFindNode(network, node1id);
                    Node node2 = createOrFindNode(network, node2id);
                    node1.addNeighbor(node2);
                }
            }
            line = br.readLine();
        }
        return network;
    }

    /**
     * Return the node identified by id if it exists in the network, otherwise create it.
     * @param network
     * @param id
     * @return
     */
    private Node createOrFindNode(Network network, String id) {
        if (network.getNode(id) == null) {
            Node node = new NodeImpl(id);
            network.addNode(node);
            return node;
        } else {
            return network.getNode(id);
        }
    }
}
