package edu.ohsu.graphlet.cytoscape;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

import cytoscape.CyNetwork;
import cytoscape.task.TaskMonitor;
import edu.ohsu.graphlet.core.CountResults;
import edu.ohsu.graphlet.core.GraphletCounter;
import edu.ohsu.graphlet.core.HasGraphletSignature;
import edu.ohsu.graphlet.core.Node;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Cytoscape task for counting graphlet signatures for a set of nodes that have been
 * selected through the Cytoscape UI.
 */
public class CountGraphletsForSelectedNodesTask extends GraphletCountTask {
    private TaskMonitor taskMonitor = null;
    private CyNetwork network;
    private Set selectedNodes;
    private boolean interrupted;
    private GraphletCountResults results;

    /**
     * Instantiates a new task given the set of selected nodes and the CyNetwork.
     * @param selectedNodes
     * @param network
     */
    public CountGraphletsForSelectedNodesTask(Set selectedNodes, CyNetwork network) {
        this.selectedNodes = selectedNodes;
        this.network = network;
    }

    public GraphletCountResults getResults() {
        return results;
    }

    /**
     * Executes the graphlet counting task and places the results in results.
     */
    public void run() {
        results = new GraphletCountResults();
        CountResults countsByNode = new CountResults();
        GraphletCounter counter = new GraphletCounter();
        int i = 1;

        for (Iterator iterator = selectedNodes.iterator(); iterator.hasNext();) {
            if (interrupted) return;
            taskMonitor.setStatus("Computing for node " + i + "/" + selectedNodes.size());
            taskMonitor.setPercentCompleted((int) (((double) (i) / selectedNodes.size()) * 100));
            giny.model.Node selectedCyNode = (giny.model.Node) iterator.next();
            Node myNode = new CyNodeImpl(network, selectedCyNode);
            myNode.setCounts(counter.getGraphletSignature(myNode));
            countsByNode.put(selectedCyNode.getIdentifier(), myNode);
            i++;
        }
        results.setCountsByNode(countsByNode);

        double[] meanCounts = calculateMeanCounts(countsByNode.values());
        results.setMeans(meanCounts);
    }

    public void halt() {
        interrupted = true;
    }

    public void setTaskMonitor(TaskMonitor taskMonitor) throws IllegalThreadStateException {
        if (this.taskMonitor != null) {
            throw new IllegalStateException("Task Monitor is already set.");
        }
        this.taskMonitor = taskMonitor;    
    }

    public String getTitle() {
        return "Counting Graphlets";
    }
}
