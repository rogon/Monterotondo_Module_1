package edu.ohsu.graphlet.core;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MotifInstanceCollapserTest {

    @Test
    public void testCollapseMotif() {
        MotifInstanceCollapser motifInstanceCollapser = new MotifInstanceCollapser();

        Network network = new NetworkImpl();
        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");
        network.addNode(a);
        network.addNode(b);
        network.addNode(c);
        d.addNeighbor(c);
        d.addNeighbor(a);
        e.addNeighbor(a);

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(a);

        assertNotNull(network.getNode("a"));
        MotifInstance motifInstance = new MotifInstance("a", "b", "c");

        motifInstanceCollapser.collapseNodes(motifInstance, network);
        Node newNode = network.getNode("a-b-c");
        assertNotNull(newNode);
        assertNull(network.getNode("a"));
        assertNull(network.getNode("b"));
        assertNull(network.getNode("c"));
        assertTrue(newNode.getNeighbors().contains(d));
        assertTrue(newNode.getNeighbors().contains(e));
    }

    @Test
    public void testCollapseAndRestoreNetwork() {
        MotifInstanceCollapser motifInstanceCollapser = new MotifInstanceCollapser();

        NetworkImpl network = new NetworkImpl();
        Node a = new NodeImpl("a");
        Node b = new NodeImpl("b");
        Node c = new NodeImpl("c");
        Node d = new NodeImpl("d");
        Node e = new NodeImpl("e");
        network.addNode(a);
        network.addNode(b);
        network.addNode(c);
        network.addNode(d);
        network.addNode(e);
        d.addNeighbor(c);
        d.addNeighbor(a);
        e.addNeighbor(a);

        a.addNeighbor(b);
        b.addNeighbor(c);
        c.addNeighbor(a);

        assertNotNull(network.getNode("a"));
        MotifInstance motifInstance = new MotifInstance("a", "b", "c");

        motifInstanceCollapser.collapseNodes(motifInstance, network);
        Node newNode = network.getNode("a-b-c");
        assertNotNull(newNode);
        assertNull(network.getNode("a"));
        assertNull(network.getNode("b"));
        assertNull(network.getNode("c"));
        assertTrue(newNode.getNeighbors().contains(d));
        assertTrue(newNode.getNeighbors().contains(e));

        network.removeNode(newNode.getId());
        for (String member : motifInstance.getMembers()) {
            network.restoreNode(member);
        }

        assertEquals(4, a.getNeighbors().size()); // d, e, b, c
        assertEquals(2, b.getNeighbors().size()); // a, c
        assertEquals(3, c.getNeighbors().size()); // a, b, d
        assertEquals(1, e.getNeighbors().size()); // a
    }
}
