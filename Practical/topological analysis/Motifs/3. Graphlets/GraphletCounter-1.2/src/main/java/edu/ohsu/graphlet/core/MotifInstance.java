package edu.ohsu.graphlet.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents a set of connected nodes, referred to herein as a motif, in a network.
 */
public class MotifInstance extends BaseGraphletSignature {
    private static final String NODE_ID_DELIMITER = "-";

    /**
     * A list of the String ids of the nodes that are members of this motif.
      */
    private List<String> members = new ArrayList<String>();

    private Motif motif;

    public Motif getMotif() {
        return motif;
    }

    public void setMotif(Motif motif) {
        this.motif = motif;
    }

    public MotifInstance() {}

    /**
     * Creates a motif membership structure containing the nodes referred to by the nodeIds.
     * @param nodeIds
     */
    public MotifInstance(String... nodeIds) {
        for (String memString : nodeIds) {
            members.add(memString);
        }
    }

    /**
     * Creates a motif membership structure containing the nodes referred to by the nodeIds.
     * @param nodeIds
     */
    public MotifInstance(List<String> nodeIds) {
        this.members = nodeIds;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    /**
     * Builds a user-friendly name representing the motif.
     * @return A string with the ids of the nodes in the motif concatenated together with NODE_ID_DELIMITER
     */
    public String getMotifInstanceName() {
        boolean first = true;
        StringBuffer name = new StringBuffer();
        for (String nodeId : getMembers()) {
            if (! first) {
                name.append(NODE_ID_DELIMITER);
            }
            first = false;
            name.append(nodeId);
        }
        return name.toString();
    }

    public String getId() {
        return getMotifInstanceName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MotifInstance)) return false;

        MotifInstance that = (MotifInstance) o;

        if (members != null ? !members.equals(that.members) : that.members != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return members != null ? members.hashCode() : 0;
    }

    public boolean containsNode(String id) {
        for (String n : members) {
            if (n.equals(id)) return true;
        }
        return false;
    }
}
