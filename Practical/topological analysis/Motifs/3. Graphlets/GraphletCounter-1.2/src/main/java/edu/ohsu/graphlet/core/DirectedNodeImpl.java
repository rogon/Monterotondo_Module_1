package edu.ohsu.graphlet.core;

import java.util.Set;
import java.util.HashSet;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class is under development and not in use yet!!
 *
 * This class represents a node in a directed graph.
 */
public class DirectedNodeImpl extends BaseGraphletSignature implements Node {
    private String id;
    private Set<Node> neighbors = new HashSet<Node>();

    public DirectedNodeImpl(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Set<Node> getNeighbors() {
        return neighbors;
    }

    public void addNeighbor(Node n) {
        neighbors.add(n);
    }

    public String toString() { return id; }

    public void removeEdge(Node neighbor) {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DirectedNodeImpl)) return false;

        DirectedNodeImpl node = (DirectedNodeImpl) o;

        if (id != null ? !id.equals(node.id) : node.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
