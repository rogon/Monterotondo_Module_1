Network Biology Fundamentals

Schedule details:
------------------------------------------------------------------------------
Day 1
------------------------------------------------------------------------------

Theory:
09:00-10:00 - Introduction to graph theory
10:00-12:00 - Topological network analysis
13:00-14:30 - Introduction to Cytoscape
14:30-15:30 - Visualizing data with networks
15:30-16:00 - BioMart

Practical:
1. Navigating Cytoscape
	Loading a Simple Network
	Manipulating Your Network
	Integrating experimental data (attribute import)

2. Visualizing data with networks
	Visual Styles
	Network Layouts
	Data-driven customization of CS graphics

3. Filtering, merging, de-duplicating, and editing

4. App Manager - plugins in Cytoscape

5. How to annotate your data with BioMart – converting id’s, enriching data with e.g. PFAM.


