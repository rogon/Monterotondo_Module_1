package edu.ohsu.graphlet.cytoscape;

import edu.ohsu.graphlet.core.CountResults;
import edu.ohsu.graphlet.core.GraphletCounter;
import edu.ohsu.graphlet.core.HasGraphletSignature;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class is a container for the results returned by a graphlet counting task, including the individual
 * graphlet counts for each node or motif selected, and an array of the mean count for each automorphism
 * orbit across each node or set of nodes counted.
 */
public class GraphletCountResults {
    /**
     * Individual graphlet count results for each node or set of nodes in the task.
     */
    private CountResults countsByNode;

    /**
     * The mean count for each of the 73 automorphism orbits across all selected nodes or sets of nodes in the task
     */
    private double[] means;

    public CountResults getCountsByNodeId() {
        return countsByNode;
    }

    public void setCountsByNode(CountResults countsByNode) {
        this.countsByNode = countsByNode;
    }

    public double[] getMeans() {
        return means;
    }

    public double[] getWeightedMeans() {
        double[] weightedMeans = new double[GraphletCounter.NUM_ORBITS];
        for (int i = 0; i < GraphletCounter.NUM_ORBITS; i++) {
            weightedMeans[i] = GraphletCounter.orbitWeights[i] * means[i];
        }
        return weightedMeans;
    }

    public void setMeans(double[] means) {
        this.means = means;
    }

    public HasGraphletSignature getGraphletSignature(String selection) {
        return countsByNode.get(selection);
    }
}
